package models;

public class Artista {

    public Integer id;
    public String nombre;
    public String pais;
    public String generoMusical;
    public String genero;
    public String email;



    public Artista(String nombre, String pais, String generoMusical, String genero, String email){
        this.nombre = nombre;
        this.pais = pais;
        this.generoMusical = generoMusical;
        this.genero = genero;
        this.email = email;
    }

    public Artista(){

    }

}
