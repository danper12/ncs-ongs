package controllers;
import models.Artista;
import play.api.data.Form;
import play.data.FormFactory;
import play.mvc.*;

import javax.inject.Inject;


public class ArtistaController extends Controller {

    @Inject FormFactory formFactory;

    public Result index(){

        return TODO;
    }

    //Crear artista
    public Result create(){

        Form<Artista> artisform = formFactory.form(Artista.class);
        return ok(create.render(artisform));
    }

    //Editar artista
    public Result edit(Integer id){

        return TODO;
    }

    //Guardar artista

    public Result save(){
        return TODO;
    }

    //Actualizar artista

    public Result update(){

        return TODO;
    }

    //Eliminar artista

    public Result destroy(Integer id){
        return TODO;
    }

    //Mostrar artistas

    public Result show(Integer id){
        return TODO;
    }




}
