
package views.html.ArtistaViews

import _root_.play.twirl.api.TwirlFeatureImports._
import _root_.play.twirl.api.TwirlHelperImports._
import _root_.play.twirl.api.Html
import _root_.play.twirl.api.JavaScript
import _root_.play.twirl.api.Txt
import _root_.play.twirl.api.Xml
import models._
import controllers._
import play.api.i18n._
import views.html._
import play.api.templates.PlayMagic._
import java.lang._
import java.util._
import scala.collection.JavaConverters._
import play.core.j.PlayMagicForJava._
import play.mvc._
import play.api.data.Field
import play.mvc.Http.Context.Implicit._
import play.data._
import play.core.j.PlayFormsMagicForJava._

object create extends _root_.play.twirl.api.BaseScalaTemplate[play.twirl.api.HtmlFormat.Appendable,_root_.play.twirl.api.Format[play.twirl.api.HtmlFormat.Appendable]](play.twirl.api.HtmlFormat) with _root_.play.twirl.api.Template1[Form[Artista],play.twirl.api.HtmlFormat.Appendable] {

  /**/
  def apply/*1.2*/(artisform : Form[Artista]):play.twirl.api.HtmlFormat.Appendable = {
    _display_ {
      {
/*2.2*/import helper._


Seq[Any](format.raw/*1.29*/("""
"""),format.raw/*3.1*/("""



"""),format.raw/*7.1*/("""<html>
    <head>
        <title>Crear artista</title>
    </head>
    <body>
        <h1>¡ Crear Artista !</h1>

        """),_display_(/*14.10*/helper/*14.16*/.form(action =  routes.ArtistaController.save())/*14.64*/{_display_(Seq[Any](format.raw/*14.65*/("""
            """),_display_(/*15.14*/helper/*15.20*/.inputText(artisform("Nombre"))),format.raw/*15.51*/("""
            """),_display_(/*16.14*/helper/*16.20*/.inputText(artisform("Correo "))),format.raw/*16.52*/("""
            """),_display_(/*17.14*/helper/*17.20*/.inputText(artisform("Sexo"))),format.raw/*17.49*/("""
            """),_display_(/*18.14*/helper/*18.20*/.inputText(artisform("Genero Musical"))),format.raw/*18.59*/("""
            """),_display_(/*19.14*/helper/*19.20*/.inputText(artisform("Pais"))),format.raw/*19.49*/("""

            """),format.raw/*21.13*/("""<input type="submit" value="Crear Artista">

        """)))}),format.raw/*23.10*/("""

    """),format.raw/*25.5*/("""</body>
</html>"""))
      }
    }
  }

  def render(artisform:Form[Artista]): play.twirl.api.HtmlFormat.Appendable = apply(artisform)

  def f:((Form[Artista]) => play.twirl.api.HtmlFormat.Appendable) = (artisform) => apply(artisform)

  def ref: this.type = this

}


              /*
                  -- GENERATED --
                  DATE: Mon Aug 28 16:24:15 COT 2017
                  SOURCE: C:/repos/ncs-ongs/app/views/ArtistaViews/create.scala.html
                  HASH: dfcc5fb5cb40431cab05bba669f6525a6c9e94c3
                  MATRIX: 969->1|1069->31|1114->28|1142->48|1176->56|1333->186|1348->192|1405->240|1444->241|1486->256|1501->262|1553->293|1595->308|1610->314|1663->346|1705->361|1720->367|1770->396|1812->411|1827->417|1887->456|1929->471|1944->477|1994->506|2038->522|2125->578|2160->586
                  LINES: 28->1|31->2|34->1|35->3|39->7|46->14|46->14|46->14|46->14|47->15|47->15|47->15|48->16|48->16|48->16|49->17|49->17|49->17|50->18|50->18|50->18|51->19|51->19|51->19|53->21|55->23|57->25
                  -- GENERATED --
              */
          