
// @GENERATOR:play-routes-compiler
// @SOURCE:C:/repos/ncs-ongs/conf/routes
// @DATE:Mon Aug 28 15:41:12 COT 2017


package router {
  object RoutesPrefix {
    private var _prefix: String = "/"
    def setPrefix(p: String): Unit = {
      _prefix = p
    }
    def prefix: String = _prefix
    val byNamePrefix: Function0[String] = { () => prefix }
  }
}
